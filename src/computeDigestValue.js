/* eslint-env browser, serviceworker */
/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "computeDigestValue" }] */
/* global computeHashValue, BitCoder */

// https://tools.ietf.org/html/draft-kazuho-h2-cache-digest-01#section-2.1.1
// 2.1.1.  Computing the Digest-Value

// Given the following inputs:
async function computeDigestValue (
  // "validators", a boolean indicating whether validators ([RFC7232])
  // are to be included in the digest;
  validators = false,

  // "URLs'", an array of (string "URL", string "ETag") tuples, each
  // corresponding to the Effective Request URI ([RFC7230],
  // Section 5.5) of a cached response [RFC7234] and its entity-tag
  // [RFC7232] (if "validators" is true and if the ETag is available;
  // otherwise, null);
  urls = [],

  // "P", an integer that MUST be a power of 2 smaller than 2**32, that
  // indicates the probability of a false positive that is acceptable,
  // expressed as "1/P".
  p
) {
  if (p >= 2 ** 32) throw Error(`Invalid probability: "${p}" must be smaller than 2**32`)
  if (!isPowerOfTwo(p)) throw Error(`Invalid probability: "${p}" must be a power of 2`)

  //  "digest-value" can be computed using the following algorithm:
  let digestValue

  // Let N be the count of "URLs"' members, rounded to the nearest
  // power of 2 smaller than 2**32.
  const n = Math.min(2 ** Math.round(Math.log2(urls.length)), 2 ** 31)

  // Let "hash-values" be an empty array of integers.
  let hashValues = []

  // Append 0 to "hash-values".
  // hashValues.push(0) // BitCoder.prototype.gcsEncode handles this by skipping the first entry.

  // For each ("URL", "ETag") in "URLs", compute a hash value
  // (Section 2.1.2) and append the result to "hash-values".
  hashValues = hashValues.concat(
    await Promise.all(
      urls.map((tuple) => computeHashValue(tuple, validators, n, p))
    )
  )

  // Sort "hash-values" in ascending order.
  .sort(ascendingOrderComparator)

  // console.dir({n: Math.log2(n), p: Math.log2(p), hashValues})

  // Let "digest-value" be an empty array of bits.
  digestValue = Uint8Array.from(
    new BitCoder()

      // Write log base 2 of "N" to "digest-value" using 5 bits.
      .addBits(Math.log2(n), 5)

      // Write log base 2 of "P" to "digest-value" using 5 bits.
      .addBits(Math.log2(p), 5)

      // For each "V" in "hash-values":
      //   1.  Let "W" be the value following "V" in "hash-values".
      //   2.  If "W" and "V" are equal, continue to the next "V".
      //   3.  Let "D" be the result of "W - V - 1".
      //   4.  Let "Q" be the integer result of "D / P".
      //   5.  Let "R" be the result of "D modulo P".
      //   6.  Write "Q" '0' bits to "digest-value".
      //   7.  Write 1 '1' bit to "digest-value".
      //   8.  Write "R" to "digest-value" as binary, using log2("P"5)
      //       bits.
      .gcsEncode(hashValues, Math.log2(p))

      .value
  )

  return digestValue
}

// LMGTFY
function isPowerOfTwo (x) {
  return ((x > 0) && ((x & (~x + 1)) === x))
}

function ascendingOrderComparator (a, b) {
  return a - b
}
