/* eslint-env browser, serviceworker */
/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "ephemeral" }] */

function ephemeral (worker, expiration = 1000) {
  let value
  let ready = false
  let working = false
  return async (...args) => {
    if (working) return await working
    if (ready) return value
    working = Promise.resolve(worker(...args))
    value = await working
    ready = true
    working = false
    setTimeout(() => {
      ready = false
      value = undefined
    }, expiration)
    return value
  }
}
