/* eslint-env browser, serviceworker */
/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "uint8ToBase64" }] */

function uint8ToBase64 (buffer) {
  var binary = ''
  var len = buffer.byteLength
  for (var i = 0; i < len; i++) {
    binary += String.fromCharCode(buffer[i])
  }
  return btoa(binary)
    // Trimming Base64 padding is required by H2O's decoder.
    .replace(/=+$/, '')
}
