/* eslint-env browser, serviceworker */
/* global ephemeral, computeDigestValue, uint8ToBase64 */

importScripts(
  'ephemeral.js',
  'computeDigestValue.js',
  'computeHashValue.js',
  'uint8ToBase64.js',
  'BitCoder.js'
)

const cacheName = '🦄'
let cache

addEventListener('activate', (event) => {
  event.waitUntil((async () => {
    cache = await caches.open(cacheName)
  })())
})

addEventListener('fetch', (event) => {
  if (event.request.method === 'GET') {
    event.respondWith(respond(event.request))
  }
})

async function respond (request) {
  if (!cache) cache = await caches.open(cacheName)
  const cached = await cache.match(request)
  if (cached) {
    if (isFresh(cached)) {
      log(request.url, 'cache hit')
      return cached
    } else cache.delete(cached)
  }

  log(request.url, 'cache miss')
  const digest = await retrieveDigest()
  const outbound = new Request(request)
  outbound.headers.set('cache-digest', digest)
  const response = await fetch(outbound)
  if (isCacheable(response)) store(request, response.clone())
  return response
}

function log (url, message) {
  // const {pathname, search} = new URL(url)
  // const cleanUrl = decodeURI(pathname + search)
  // console.info(message, cleanUrl)
}

async function store (request, response) {
  await cache.put(request, response)
}

const retrieveDigest = ephemeral(async function retrieveDigest () {
  const urls = await immutableCachedUrls()
  const digest = await computeDigestValue(
    false, // validators
    urls.map((url) => [url, null]), // tuples [url, etag|null]
    2 ** 7 // probability (1/P)
  )
  const encoded = `${uint8ToBase64(digest)}; complete`
  return encoded
})

async function immutableCachedUrls () {
  const urls = []
  const now = Date.now()
  const requests = await cache.keys()
  for (const request of requests) {
    const response = await cache.match(request)
    if (!response) continue
    const cacheControl = parseCacheControlHeader(response)
    if (!isFresh(response, now, cacheControl)) {
      cache.delete(request)
    } else if (cacheControl.has('immutable')) {
      urls.push(response.url)
    }
  }
  return urls
}

function parseCacheControlHeader (response) {
  const directives = new Map()
  if (response.headers.has('cache-control')) {
    const header = response.headers.get('cache-control')
    for (const chunk of header.split(',')) {
      const extension = chunk.trim()
      if (extension.includes('=')) {
        const [key, ...values] = extension.split('=')
        directives.set(key, values.join('='))
      } else directives.set(extension, true)
    }
  }
  return directives
}

function isFresh (response, now = Date.now(), directives = parseCacheControlHeader(response)) {
  const EXPIRED = false
  const FRESH = true

  const date = response.headers.has('date')
    ? Date.parse(response.headers.get('date'))
    : NaN

  if (response.headers.has('expires')) {
    const expires = Date.parse(response.headers.get('expires'))
    if (now > expires) return EXPIRED
  }

  if (directives.has('max-age')) {
    const maxAge = parseInt(directives.get('max-age'), 10) * 1e3
    if (now > date + maxAge) return EXPIRED
  }

  if (directives.has('no-store') || directives.has('no-cache')) {
    return EXPIRED
  }

  return FRESH
}

function isCacheable (response) {
  const directives = parseCacheControlHeader(response)
  return directives.has('immutable') && allowCaching(directives)
}

function allowCaching (directives) {
  const ALLOW = true
  const DISALLOW = false

  if (directives.has('no-store')) return DISALLOW
  else if (directives.has('max-age')) {
    const maxAge = parseInt(directives.get('max-age'), 10)
    if (maxAge === 0) return DISALLOW
  }

  return ALLOW
}
