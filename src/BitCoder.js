/*
 * Copyright (c) 2015,2016 Jxck, DeNA Co., Ltd., Kazuho Oku
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

/* eslint-disable */

function BitCoder() {
  this.value = [];
  this.leftBits = 0;
}

BitCoder.prototype.addBit = function (b) {
  if (this.leftBits == 0) {
    this.value.push(0);
    this.leftBits = 8;
  }
  --this.leftBits;
  if (b)
    this.value[this.value.length - 1] |= 1 << this.leftBits;
  return this;
};

BitCoder.prototype.addBits = function (v, nbits) {
  if (nbits != 0) {
    do {
      --nbits;
      this.addBit(v & (1 << nbits));
    } while (nbits != 0);
  }
  return this;
};

BitCoder.prototype.gcsEncode = function (values, bits_fixed) {
  // values = values.sort(function (a, b) { return a - b; });
  var prev = -1;
  for (var i = 0; i != values.length; ++i) {
    if (prev == values[i])
      continue;
    var v = values[i] - prev - 1;
    for (var q = v >> bits_fixed; q != 0; --q)
      this.addBit(0);
    this.addBit(1);
    this.addBits(v, bits_fixed);
    prev = values[i];
  }
  return this;
};

/* eslint-enable */
