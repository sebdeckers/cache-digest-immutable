/* eslint-env browser, serviceworker */
/* eslint no-unused-vars: ["error", { "varsIgnorePattern": "computeHashValue" }] */

// https://tools.ietf.org/html/draft-kazuho-h2-cache-digest-01#section-2.1.2
// 2.1.2.  Computing a Hash Value

// Given:
const textEncoder = new TextEncoder('utf-8')
async function computeHashValue (
  [
    // "URL", an array of characters
    url,

    // "ETag", an array of characters
    etag
  ],

  // "validators", a boolean
  validators,

  // "N", an integer
  n,

  // "P", an integer
  p
) {
  // "hash-value" can be computed using the following algorithm:
  let hashValue

  // Let "key" be "URL" converted to an ASCII string by percent-
  // encoding as appropriate [RFC3986].
  const key = appropriatelyPercentEncode(url)

  // If "validators" is true and "ETag" is not null:
  if (validators === true && etag !== null) {
    // Append "ETag" to "key" as an ASCII string, including both the
    // "weak" indicator (if present) and double quotes, as per
    // [RFC7232] Section 2.3.
    throw Error('Etags are not supported in this implementation') // TODO: Not sure how this works. Examples?
  }

  // Let "hash-value" be the SHA-256 message digest [RFC6234] of
  // "key", expressed as an integer.
  hashValue = new DataView(await crypto.subtle.digest(
    'SHA-256',
    textEncoder.encode(key)
  )).getUint32(0) // TODO: Spec allows up to 62 bits (n=2**31, p=2**31)

  // Truncate "hash-value" to log2( "N" * "P" ) bits.
  const truncate = Math.log2(n * p)
  if (truncate > 31) throw Error('This implementation only supports up to 31 bit hash values')
  hashValue = (hashValue >> (32 - truncate)) & ((1 << truncate) - 1)

  return hashValue
}

// https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/encodeURIComponent
// "To be more stringent in adhering to RFC 3986 (which
// reserves !, ', (, ), and *), even though these
// characters have no formalized URI delimiting uses,
// the following can be safely used:"
function appropriatelyPercentEncode (url) {
  // "url" is already encoded as if it passed through encodeURI.
  // Fix any missing characters as per RFC 3986.
  return url.replace(/[!'()*]/g, function (character) {
    return '%' + character.charCodeAt(0).toString(16)
  })
}
