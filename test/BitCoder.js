/* eslint-env node */
/* globals BitCoder */

import test from 'ava'
import importScripts from 'import-scripts'

importScripts('../lib/BitCoder.js')

const fixtures = [
  {
    given: {
      data: [],
      length: 2
    },
    expected: []
  },
  {
    given: {
      data: [3, 10],
      length: 2
    },
    expected: [0b11101100]
  },
  {
    given: {
      data: [1025],
      length: 8
    },
    expected: [0b00001000, 0b00001000]
  }
]

for (const {given: {data, length}, expected} of fixtures) {
  test('BitCoder', (t) => {
    const actual = new BitCoder().gcsEncode(data, length).value
    t.deepEqual(actual, expected)
  })
}
