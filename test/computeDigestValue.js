/* eslint-env node */
/* globals computeDigestValue */

import test from 'ava'
import importScripts from 'import-scripts'
import {TextEncoder} from 'text-encoding'
import subtle from 'subtle'
import bufferDataView from 'buffer-dataview'

function digest (algo, buffer) {
  // Monkey patch until PR is merged:
  // https://github.com/rynomad/subtle/pull/20
  return subtle.digest({name: algo}, buffer)
}

global.DataView = bufferDataView
global.crypto = {subtle: {digest}}
global.TextEncoder = TextEncoder

importScripts('../lib/computeHashValue.js')
importScripts('../lib/BitCoder.js')

importScripts('../lib/computeDigestValue.js')

const fixtures = [
  {
    given: [
      false, // validators
      ['https://example.com/style.css', 'https://example.com/jquery.js']
        .map((url) => [url, null]), // tuples [url, etag|null]
      2 ** 7 // probability (1/P)
    ],
    expected: new Uint8Array([0x09, 0xd6, 0x50, 0xe0])
  }
]

for (const {given, expected} of fixtures) {
  test('computeDigestValue', async (t) => {
    const actual = await computeDigestValue(...given)
    t.deepEqual(actual, expected)
  })
}
