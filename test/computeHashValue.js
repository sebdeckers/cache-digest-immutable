/* eslint-env node */
/* globals computeHashValue */

import test from 'ava'
import importScripts from 'import-scripts'
import {TextEncoder} from 'text-encoding'
import subtle from 'subtle'
import bufferDataView from 'buffer-dataview'

function digest (algo, buffer) {
  // Monkey patch until PR is merged:
  // https://github.com/rynomad/subtle/pull/20
  return subtle.digest({name: algo}, buffer)
}

global.DataView = bufferDataView
global.crypto = {subtle: {digest}}
global.TextEncoder = TextEncoder

importScripts('../lib/computeHashValue.js')

const fixtures = [
  {
    given: [
      ['https://example.com/style.css'],
      false,
      2 ** 5,
      2 ** 7
    ],
    expected: 'baf'
  },
  {
    given: [
      [''], false, 1, 2 ** 8
    ],
    expected: 'e3'
  },
  {
    given: [
      [''], false, 1, 2 ** 5
    ],
    expected: '1c'
  },
  {
    given: [
      ['hello world'], false, 1, 2 ** 11
    ],
    expected: '5ca'
  }
]

for (const {given, expected} of fixtures) {
  test('computeHashValue', async (t) => {
    const actual = await computeHashValue(...given)
    t.is(actual.toString(16), expected)
  })
}
