/* eslint-env node */
/* globals uint8ToBase64 */

import test from 'ava'
import importScripts from 'import-scripts'
import btoa from 'btoa'

global.btoa = btoa

importScripts('../lib/uint8ToBase64.js')

const fixtures = [
  {
    given: 'hel',
    expected: 'aGVs'
  },
  {
    given: 'hell',
    expected: 'aGVsbA'
  },
  {
    given: 'hello',
    expected: 'aGVsbG8'
  }
]

for (const {given, expected} of fixtures) {
  test('uint8ToBase64', (t) => {
    const input = typeof given === 'string'
      ? Buffer.from(given, 'utf-8')
      : given
    const actual = uint8ToBase64(input)
    t.is(actual, expected)
  })
}
