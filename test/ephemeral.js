/* eslint-env node */
/* globals ephemeral */

import test from 'ava'
import importScripts from 'import-scripts'

importScripts('../lib/ephemeral.js')

async function sleep (wait) {
  return new Promise((resolve) => setTimeout(resolve, wait))
}

async function delay (fn, wait) {
  await sleep(wait)
  return await fn()
}

let callCount = 0
async function slowRandom (token) {
  callCount = callCount + 1
  const response = Math.random()
  await sleep(100)
  return [token, response]
}

const slowRandomDebounced = ephemeral(slowRandom, 300)

async function hammer () {
  const token = Math.random()
  return {
    token,
    result: await Promise.all([
      slowRandomDebounced(token),
      slowRandomDebounced(token)
    ])
  }
}

test('ephemeral', async (t) => {
  const actual = await Promise.all([
    hammer(),
    delay(hammer, 100),
    delay(hammer, 500)
  ])

  t.is(callCount, 2, 'only invoke calculation twice in total')
  t.is(3, new Set(actual.map(({token}) => token)).size, 'each time a unique token was passed')
  t.deepEqual(actual[0].result, actual[1].result, 'matching result on repeated calls within time limit')
  t.notDeepEqual(actual[1].result, actual[2].result, 'recalculate after time limit expires')
})
