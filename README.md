# 📑 Cache Digest Immutable

Uses a service worker's [`fetch` event](https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent) and [Cache API](https://developer.mozilla.org/en-US/docs/Web/API/Cache) to set the `cache-digest` header. Webservers use this digest to check which files the client aleady has cached. This avoids server-pushing redundant data, providing atomic cache invalidation when used in combination with filename revving (aka cache busting).

As an explicit opt-in, to avoid overly aggressive caching, this implementation *only* includes, in the digest, responses that have the `cache-control: immutable` extension set.

## Usage

```js
if ('serviceWorker' in navigator) {
  addEventListener('load', function () {
    navigator.serviceWorker.register('cache-digest-immutable.js')
  }, {once: true})
}
```
**Note**: This waits until everything else on the page has loaded. This is fine since the service worker only receives `fetch` events on future page loads.

## Server Support
- [H2O](https://h2o.examp1e.net) - Successfully tested with v2.1.0-beta3 ([see config](https://gitlab.com/sebdeckers/h2o_playground))
- [http2server](https://www.npmjs.com/package/http2server) - Future support expected, see [#6](https://gitlab.com/sebdeckers/http2server/issues/6)

## Specifications
- [Cache Digests for HTTP/2 `draft-kazuho-h2-cache-digest-01`](https://tools.ietf.org/html/draft-kazuho-h2-cache-digest-01)
- [HTTP Immutable Responses `draft-mcmanus-immutable-00`](https://tools.ietf.org/html/draft-mcmanus-immutable-00)

## Significant Decisions
- Written in ES2015+ which works fine as of Chrome 56 (Canary version at the time of starting this implementation).
- Uses `importScripts(...)` for native module loading within Service Workers. Not meant to be compatible with alternatives like CommonJS, ES2015 Modules (`export`/`import`), etc.
- Only caches if [`cache-control: immutable` extension](https://tools.ietf.org/html/draft-mcmanus-immutable-00) is set. This makes it opt-in for server configurations and avoids overly aggressive caching. Also minimises the cache digest size to only contain assets that will be cached long-term, as opposed to short-term ephemeral resources that require revalidation.
- Respects all caching directives by fully parsing the `date`/`expires`/`cache-control` headers (incl `max-age=N`/`no-store`/`no-cache`/`must-revalidate`).
- Manages cache by purging stale responses.
- Rewritten the URL value hashing code to use the browser's asynchronous [SubtleCrypto API's SHA256 implementation](https://developer.mozilla.org/en-US/docs/Web/API/SubtleCrypto/digest).
- Rewritten the digest algo to closely follow the spec. Still uses the same (slightly tweaked) BitCoder implementation of Golomb coding.
- Memoises the digest to avoid recalculating on every single request. Huge performance improvement by throttling only one calculation per second.
- Rewrote Base64 encoding to use browser's native [`btoa` function](https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/btoa).

## Related
- [Unbundle](https://www.npmjs.com/package/unbundle) - Originally developed for, and extracted from, this package.
- [cache-digest.js](https://github.com/h2o/cache-digest.js) - Passes the same test suite for guaranteed binary output compatibility.

## Colophon
Made by Sebastiaan Deckers in Singapore 🇸🇬
